# code-verifier-backend


## DEPENDENCIAS INSTALADAS


- Express

Permite implementar una infraestructura web flexible y rápida, ya sea para un desarrollo web clásico o implementar una RESTFul Api.

- Jest

Es un framework creado por Facebook y mantenido por la comunidad con apoyo de Facebook. Es fácil de instalar y no requiere de una configuración muy compleja para poder añadirlo en nuestros proyectos. Para realizar nuestras pruebas de testing vamos ha crear un proyecto y así poder probar todo lo que necesitemos.

- Node

Es un entorno de tiempo de ejecución en tiempo real incluye todo lo que se necesita para ejecutar un programa escrito en JavaScript

- Eslint

Es una herramienta de código abierto enfocada en el proceso de "lintig" para javascript (o más correctamente para ECMAScript). ESLint es la herramienta predominante para la tarea de "limpiar" código javascript tanto en el servidor (node. js) como en el navegador.

- Nodemon

Es una utilidad de interfaz de línea de comandos (CLI) desarrollada por @rem que envuelve su aplicación Node, vigila el sistema de archivos y reinicia automáticamente el proceso

- TypeScript 

Es un superconjunto de JavaScript, que esencialmente añade tipos estáticos y objetos basados en clases.

- Webpack

Webpack es una herramienta de compilación (una build tool) que coloca en un grafo de dependencias a todos los elementos que forman parte de tu proyecto de desarrollo




## Scripts de NPM creados

- build: 

"npx tsc"

- start:

 "node dist/index.js"
- dev: 

"concurrently \"npx tsc --watch\" \"nodemon -q dist/index.js\""

- test: 

"jest"
- serve:coverage:

 "npm run test && cd coverage/lcov-report && npx serve"

## Variables de entorno a crear en el .env 

PORT=8000

