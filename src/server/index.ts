import express, { Express, Request, Response } from 'express'
import cors from 'cors'
import helmet from 'helmet'
import swaggerUi from 'swagger-ui-express'
import rootRouter from '../routes'
import mongoose from 'mongoose'

// * Create Express APP
const server: Express = express()

// * Swagger Config and Route
server.use(
  '/docs',
  swaggerUi.serve,
  swaggerUi.setup(undefined, {
    swaggerOptions: {
      url: '/swagger.json',
      explorer: true
    }
  })
)

// * Static server
server.use(express.static('public'))

// * MongoDB Connection
mongoose.connect('mongodb://0.0.0.0/codeverification')

// * Security config
server.use(helmet())
server.use(cors())

// * Content Type Config
server.use(express.urlencoded({ extended: true, limit: '50mb' }))
server.use(express.json({ limit: '50mb' }))

// * From this point onover: http://localhost:8000/api/
server.use(
  '/api',
  rootRouter
)

// * Redirections
server.get('/', (req: Request, res: Response) => {
  res.redirect('/api')
})

export default server
