import {Get, Query, Route, Tags} from "tsoa";
import { IGoodbyeController } from "./interfaces";

import { WithDateResponse } from "./types";


@Route("api/goodbye")
@Tags("GoodbyeController")
export class GoodbyeController implements IGoodbyeController{
       /**
     * Enpoint to retreive a Message "Goodbye {name} in Json"
     * @param name  Name of user to be goodbyed
     * @returns  {BasicResponse} Promise of Basic Response
     * 
     */
        @Get("/")
    public async getMessage(@Query()name?: string): Promise<WithDateResponse> {

        const date_ob = new Date()
        let date = ("0" + date_ob.getDate()).slice(-2).toString();
        let month = ("0" + (date_ob.getMonth() + 1)).slice(-2).toString();
        let year = date_ob.getFullYear().toString();
        return{
            message: `Goodbye ${name||"nombre no definido"}`,
            date: `Fecha actual: ${date} / ${month} / ${year}`
        }
    }

    
}