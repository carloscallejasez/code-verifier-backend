import { IUser } from '../../domain/interfaces/IUser.interface'
import { BasicResponse } from "../types";
import { IKata } from '../../domain/interfaces/IKata.interface'


export interface IUserController {
  getUsers(id?: string): Promise<any>
  deleteUser(id?:string): Promise<any>
  updateUser(id: string, user: any): Promise<any>
}

export interface IHelloController {
  getMessage(name?:string): Promise<BasicResponse>
}

export interface IGoodbyeController {

  getMessage(name?:string):Promise<BasicResponse>
}

export interface IKataController {
  getAllKatas(page: number, limit: number, id?: string): Promise<any>
  getKata(page: number, limit: number, id: string) : Promise<any>
  deleteKata(id?:string): Promise<any>
  createKata(kata: IKata): Promise<any>
  updateKata(id: string, kata: IKata): Promise<any>
  filterKataByLevel(level: string): Promise<any>
  getLastFiveKatas(): Promise<any>
  getByStars(stars:number): Promise<any>
  updateStars(id: string, stars: number): Promise<any>
  getKataByIntents(intents: number): Promise<any>
}

export interface IAuthController {
  registerUser(user: IUser): Promise<any>
  loginUser(auth: any): Promise<any>
}
