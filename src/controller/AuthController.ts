import { Get, Post, Query, Route, Tags } from 'tsoa'
import { IAuthController } from './interfaces'
import { IUser } from '../domain/interfaces/IUser.interface'
import { IAuth } from '../domain/interfaces/IAuth.interface'
import { registerUser, loginUser, logoutUser } from '../domain/orm/Auth.orm'
import { LogSuccess, LogWarning } from '../utils/logger'
import { AuthResponse, ErrorResponse } from './types'
import { getUserById } from '../domain/orm/User.orm'

@Route('/api/auth')
@Tags('AuthController')
export class AuthController implements IAuthController {
  /**
   * Endpoint to create a new user
   * @param {IUser} user user to create
   * @returns message with the name of the user created
   */
  @Post('/register')
  public async registerUser (user: IUser): Promise<any> {
    let response: any = ''

    await registerUser(user).then((r) => {
      LogSuccess(`[/api/auth/register] New user registered: ${user.email}`)
      response = { message: `User created successfully: ${user.name}` }
    })

    return response
  }

  /**
   * Endpoint to login a user
   * @param {IAuth} auth authehtication credentials
   * @returns token id
   */
  @Post('/login')
  public async loginUser (auth: IAuth): Promise<any> {
    let response: AuthResponse | ErrorResponse | undefined

    if (auth) {
      LogSuccess(`[/api/auth/login] User logged in: ${auth.email}`)
      const data = await loginUser(auth)
      response = { token: data.token, message: `Welcome, ${data.user.name}` }
    } else {
      LogWarning('[/api/auth/login] Register needs auth entity (email && password)')
      response = {
        message: 'Please, provide email && password to login',
        error: '[AUTH ERROR]: Email & password are needed'
      }
    }

    return response
  }

  /**
   * Endpoint to logout
   * @returns confirmation message
   */
  @Post('/logout')
  public async logoutUser (): Promise<any> {
    let response: any = ''

    await logoutUser().then((r) => {
      LogSuccess('[/api/auth/login] User logged out')
      response = { message: 'User logged out successfully' }
    })

    return response
  }

  /**
   * Endpoint to retreive a user
   * @param {string} id Id of user to retrieve
   * @returns user found by id
   */
  @Get('/users')
  public async userData (@Query()id: string): Promise<any> {
    let response: any = ''

    LogSuccess(`[/api/users] Get user by id: ${id}`)
    response = await getUserById(id)

    return response
  }
}