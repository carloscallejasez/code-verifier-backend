import { Delete, Get, Post, Query, Route, Tags, Put } from 'tsoa'
import { IKataController } from './interfaces'
import { LogSuccess, LogWarning } from '../utils/logger'
import { deleteKata, getAllKatas, getKataById, createKata, updateKata, getLastFiveKatas, getByStars, updateStars, getKataByIntents } from '../domain/orm/Kata.orm'

@Route('/api/katas')
@Tags('KataController')
export class KataController implements IKataController {


   /**
     * Endpoint to retreive the katas in the Collection "Katas" of DB 
     * @param {string} id Id of Kata to retreive (optional)
     * @returns All katas o kata found by ID
     */
    @Get("/")
    public async getAllKatas(@Query()page: number, @Query()limit: number, @Query()id?: string): Promise<any> {
        
        let response: any = '';
        
        if(id){
            LogSuccess(`[/api/katas] Get Kata By ID: ${id} `);
            response = await getKata(id);
        }else {
            LogSuccess('[/api/katas] Get All Katas Request')
            response = await getAllKatas(page, limit);
        }
        
        return response;
    }


  /**
   * Endpoint to delete a Kata by ID from Katas collection
   * @param {string} id ID of kata to delete (optional)
   * @returns message informing if deletion was correct
   */
  @Delete('/')
  public async deleteKata (@Query()id?: string): Promise<any> {
    let response: any = ''

    if (id) {
      LogSuccess(`[/api/katas] Delete Kata By ID: ${id}`)
      await deleteKata(id).then((r:any) => {
        response = {
          message: `Kata with id ${id} deleted successfully`
        }
      })
    } else {
      LogWarning('[/api/katas] Delete Kata Request without ID')
      response = {
        message: 'Please, provide an ID to remove from database'
      }
    }

    return response
  }

  @Post('/')
  public async createKata (kata: any): Promise<any> {
    let response: any = ''

    await createKata(kata).then((r) => {
      LogSuccess(`[/api/katas] Create Kata: ${kata}`)
      response = {
        message: `Kata created successfully: ${kata.name}`
      }
    })

    return response
  }

  @Put('/')
  public async updateKata (@Query()id: string, kata: any): Promise<any> {
    let response: any = ''

    if (id) {
      LogSuccess(`[/api/katas] Update Kata By ID: ${id}`)
      await updateKata(id, kata).then((r:any) => {
        response = {
          message: `Kata with id ${id} updated successfully`
        }
      })
    } else {
      LogWarning('[/api/katas] Update Kata Request without ID')
      response = {
        message: 'Please, provide an ID to update from database'
      }
    }

    return response
  }

  /**
   * Endpoint to retrieve all Katas filtered by Level
   * @param {number} level Level of kata to filter
   * @returns All katas filtered by Level
   */
  @Get('/level')
  public async filterKataByLevel (@Query()level: string): Promise<any> {
    LogSuccess(`[/api/katas/level] Get Kata By Level: ${level}`)
    return await this.filterKataByLevel(level)
  }

  /**
   * Endpoint to retrieve 5 last Katas
   * @returns 5 last Katas
   */
  @Get('/last')
  public async getLastFiveKatas (): Promise<any> {
    LogSuccess('[/api/katas/last] Get 5 last katas')
    return await this.getLastFiveKatas()
  }

  /**
   * Endpoint to get Katas ordered by Valoration
   * @returns Katas ordered by valoration
   */
  @Get('/stars')
  public async getByStars (): Promise<any> {
    LogSuccess('[/api/katas/stars] Get Katas ordered by stars')
    return await this.getByStars()
  }

  @Put('/')
  public async updateStars (@Query()id: string, stars: number): Promise<any> {
    let response: any = ''

    if (id) {
      LogSuccess(`[/api/katas] Update Kata valoration By ID: ${id}`)
      await this.updateStars(id, stars).then((r:any) => {
        response = {
          message: `Kata's stars with id ${id} updated successfully`
        }
      })
    } else {
      LogWarning('[/api/katas] Update Kata\'s stars Request without ID')
      response = {
        message: 'Please, provide an ID to update from database'
      }
    }

    return response
  }

  /**
   * Endpoint to get Katas ordered by Intents
   * @returns Katas ordered by intents
   */
  @Get('/intents')
  public async getKataByIntents (@Query()intents: number): Promise<any> {
    LogSuccess(`[/api/katas/intents] Get Kata By Intents: ${intents}`)
    return await this.getKataByIntents(intents)
  }
}
