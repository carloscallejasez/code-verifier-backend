import { Delete, Get, Query, Route, Tags, Put } from 'tsoa'
import { IUserController } from './interfaces'
import { LogSuccess, LogWarning } from '../utils/logger'
import { deleteUserById, getAllUsers, getUserById, updateUserById } from '../domain/orm/User.orm'

@Route('/api/users')
@Tags('UserController')
export class UserController implements IUserController {
  /**
   * Endpoint to retrieve all Users or User by ID from Users collection
   * @param {string} id ID of user to retrieve (optional)
   * @returns All user or user by ID
   */
  @Get('/')
  public async getUsers (@Query()id?: string): Promise<any> {
    let response: any = ''

    if (id) {
      LogSuccess(`[/api/users] Get User By ID: ${id}`)
      response = await getUserById(id)
      response.password = ''
    } else {
      LogSuccess('[/api/users] Get All Users Request')
      response = await getAllUsers()
    }

    return response
  }

  /**
   * Endpoint to delete a User by ID from Users collection
   * @param {string} id ID of user to delete (optional)
   * @returns message informing if deletion was correct
   */
  @Delete('/')
  public async deleteUser (@Query()id?: string): Promise<any> {
    let response: any = ''

    if (id) {
      LogSuccess(`[/api/users] Delete User By ID: ${id}`)
      await deleteUserById(id).then((r) => {
        response = {
          status: 204,
          message: `User with id ${id} deleted successfully`
        }
      })
    } else {
      LogWarning('[/api/users] Delete User Request without ID')
      response = {
        status: 400,
        message: 'Please, provide an ID to remove from database'
      }
    }

    return response
  }

  @Put('/')
  public async updateUser (@Query()id: string, user: any): Promise<any> {
    let response: any = ''

    if (id) {
      LogSuccess(`[/api/users] Update User By ID: ${id}`)
      await updateUserById(id, user).then((r) => {
        response = {
          status: 204,
          message: `User with id ${id} updated successfully`
        }
      })
    } else {
      LogWarning('[/api/users] Update User Request without ID')
      response = {
        status: 400,
        message: 'Please, provide an ID to update from database'
      }
    }

    return response
  }
}
