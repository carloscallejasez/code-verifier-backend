import express, { Request, Response } from 'express'
import bcrypt from 'bcrypt'
import bodyParser from 'body-parser'
import { AuthController } from '../controller/AuthController'
import { IUser } from '../domain/interfaces/IUser.interface'
import { IAuth } from '../domain/interfaces/IAuth.interface'
import { verifyToken } from '../middlewares/verifyToken.middleware'

const authRouter = express.Router()
const jsonParser = bodyParser.json()
const controller: AuthController = new AuthController()

authRouter.route('/register')
  .post(jsonParser, async (req: Request, res: Response) => {
    const { name, email, password, age } = req?.body
    let hashedPassword = ''

    if (name && password && email && age) {
      hashedPassword = bcrypt.hashSync(password, 8)
      const newUser: IUser = {
        name: name,
        email: email,
        password: hashedPassword,
        age: age,
        katas: []
      }
      const response: any = await controller.registerUser(newUser)
      return res.status(200).send(response)
    } else {
      return res.status(404).send({ message: '[ERROR User data missing]: No user can be registered' })
    }
  })

authRouter.route('/login')
  .post(jsonParser, async (req: Request, res: Response) => {
    const { email, password } = req?.body

    if (email && password) {
      const auth: IAuth = { email: email, password: password }
      const response: any = await controller.loginUser(auth)
      return res.status(200).send(response)
    } else {
      return res.status(404).send({ message: '[ERROR User Data missing]: User can be logged in' })
    }
  })

// TODO: Implement logout router after implementing logout method

authRouter.route('/users')
  .get(verifyToken, jsonParser, async (req: Request, res: Response) => {
    const { id } = req?.body

    if (id) {
      const response: any = await controller.userData(id)
      return res.status(200).send(response)
    } else {
      return res.status(401).send({ mesasage: 'You are not authorised to perform this action' })
    }
  })

export default authRouter;