import { IUser } from '../domain/interfaces/IUser.interface'
import express, { Request, Response } from 'express'
import { UserController } from '../controller/UsersController'
import { verifyToken } from '../middlewares/verifyToken.middleware'
import bcrypt from 'bcrypt'
import bodyParser from 'body-parser'

const usersRouter = express.Router()
const jsonParser = bodyParser.json()
const controller: UserController = new UserController()

usersRouter.route('/')
  .get(verifyToken, jsonParser, async (req: Request, res: Response) => {
    const { page, limit, id } = req?.body
    const response: any = await controller.getUsers(page, limit, id)
    return res.status(200).send(response)
  })
  .delete(verifyToken, jsonParser, async (req: Request, res: Response) => {
    const { id } = req?.body
    const response: any = await controller.deleteUser(id)
    return res.status(response.status).send(response)
  })
  .put(verifyToken, jsonParser, async (req: Request, res: Response) => {
    const { id, name, email, age, password, katas } = req?.body
    let hashedPassword = ''

    hashedPassword = bcrypt.hashSync(password, 8)

    const user: IUser = {
      name: name,
      email: email,
      age: age,
      password: hashedPassword,
      katas: katas
    }

    if (!password) user.password = password

    const response: any = await controller.updateUser(id, user)
    return res.status(200).send(response)
  })

usersRouter.route('/katas')
  .get(verifyToken, jsonParser, async (req: Request, res: Response) => {
    const { page, limit, id } = req?.body
    const response: any = await controller.getKatas(page, limit, id)
    return res.status(200).send(response)
  })

export default usersRouter