import express, { Request, Response } from 'express'
import { KataController } from '../controller/KatasController'
import { verifyToken } from '../middlewares/verifyToken.middleware'
import bodyParser from 'body-parser'
import { IKata } from '../domain/interfaces/IKata.interface'

const katasRouter = express.Router()
const jsonParser = bodyParser.json()
const controller: KataController = new KataController()

katasRouter.route('/')
  .get(verifyToken, jsonParser, async (req: Request, res: Response) => {
    const { page, limit, id } = req?.body
    const response: any = await controller.getAllKatas(page, limit, id)
    return res.send(response)
  })
  .delete(verifyToken, jsonParser, async (req: Request, res: Response) => {
    const { id } = req?.body
    const response: any = await controller.deleteKata(id)
    return res.send(response)
  })
  .post(verifyToken, jsonParser, async (req: Request, res: Response) => {
    const { name, description, level, intents, stars, numStars, creator, solution, participants } = req?.body

    const kata: IKata = {
      name: name,
      description: description,
      level: level,
      intents: intents,
      stars: stars,
      numStars: numStars,
      creator: creator,
      solution: solution,
      participants: participants
    }

    const response: any = await controller.createKata(kata)
    return res.send(response)
  })
  .put(verifyToken, jsonParser, async (req: Request, res: Response) => {
    const { id, name, description, level, intents, stars, numStars, creator, solution, participants } = req?.body
    const newDesc = ''

    const kata: IKata = {
      name: name,
      description: newDesc,
      level: level,
      intents: intents,
      stars: stars,
      numStars: numStars,
      creator: creator,
      solution: solution,
      participants: participants
    }

    if (!description) {
      kata.description = description
    }

    const response: any = await controller.updateKata(id, kata)
    return res.send(response)
  })

katasRouter.route('/level')
  .get(verifyToken, jsonParser, async (req: Request, res: Response) => {
    const { level } = req?.body
    const response: any = await controller.filterKataByLevel(level)
    return res.send(response)
  })

katasRouter.route('/last')
  .get(verifyToken, async (req: Request, res: Response) => {
    const response: any = await controller.getLastFiveKatas()
    return res.send(response)
  })

katasRouter.route('/stars')
  .get(verifyToken, async (req: Request, res: Response) => {
    const response: any = await controller.getByStars()
    return res.send(response)
  })
  .put(verifyToken, jsonParser, async (req: Request, res: Response) => {
    const { id, stars } = req?.body
    const response: any = await controller.updateStars(id, stars)
    return res.send(response)
  })

katasRouter.route('/intents')
  .get(verifyToken, jsonParser, async (req: Request, res: Response) => {
    const { intents } = req?.body
    const response: any = await controller.getKataByIntents(intents)
    return res.send(response)
  })

export default katasRouter