import { IKata } from './IKata.interface'

export interface IUser {
  name: string,
  email: string,
  age: Number,
  password: string,
  katas: IKata[]
}