import { userEntity } from '../entities/User.entity'
import { kataEntity } from '../entities/Kata.entity'
import { LogError } from '../../utils/logger'
import { IUser } from '../interfaces/IUser.interface'
import { IKata } from '../interfaces/IKata.interface'

/**
 * Obtain all users
 */
export const getAllUsers = async (page: number, limit: number): Promise<any[] | undefined> => {
  try {
    const userModel = userEntity()
    const response: any = {}

    await userModel
      .find()
      .select('name email age katas')
      .limit(limit)
      .skip((page - 1) * limit)
      .exec()
      .then((users: IUser[]) => { response.users = users })

    await userModel
      .countDocuments()
      .then((total: number) => {
        response.totalPages = Math.ceil(total / limit)
        response.currentPage = page
      })

    return response
  } catch (error) {
    LogError(`[ORM ERROR]: Getting all users: ${error}`)
  }
}

/**
 * Obtain all katas by user id
 */
export const getAllKatasFromUser = async (page: number, limit: number, id: string): Promise<any[] | undefined> => {
  try {
    const userModel = userEntity()
    const kataModel = kataEntity()
    const response: any = {}

    await userModel
      .findById(id)
      .then(async (user: IUser) => {
        await kataModel
          .find({ ':_id': { $in: user.katas } })
          .then((katas: IKata[]) => { response.katas = user.katas })
      }).catch((error) => {
        LogError(`[ORM ERROR]: Getting all katas by user id: ${error}`)
      })

    return response
  } catch (error) {
    LogError(`[ORM ERROR]: Getting all katas by user id: ${error}`)
  }
}

/**
 * Obtain user by id
 */
export const getUserById = async (id: string) : Promise<any | undefined> => {
  try {
    return await userEntity().findById(id).select('name email age katas')
  } catch (error) {
    LogError(`[ORM ERROR]: Getting user by id: ${error}`)
  }
}

/**
 * Delete user by id
 */
export const deleteUserById = async (id: string) : Promise<any | undefined> => {
  try {
    return await userEntity().deleteOne({ _id: id })
  } catch (error) {
    LogError(`[ORM ERROR]: Deleting user by id: ${error}`)
  }
}

/**
 * Create user
 */
export const createUser = async (user: any): Promise<any | undefined> => {
  try {
    return await userEntity().create(user)
  } catch (error) {
    LogError(`[ORM ERROR]: Creating user: ${error}`)
  }
}

/**
 * Update user by id
 */
export const updateUserById = async (id: string, user: any): Promise<any | undefined> => {
  try {
    return await userEntity().findByIdAndUpdate(id, user)
  } catch (error) {
    LogError(`[ORM ERROR]: Updating user ${id}: ${error}`)
  }
}