import { userEntity } from '../entities/User.entity'
import { LogError } from '../../utils/logger'
import { IUser } from '../interfaces/IUser.interface'
import { IAuth } from '../interfaces/IAuth.interface'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import dotenv from 'dotenv'

dotenv.config()
const secret: any = process.env.SECRETKEY

// Register User
export const registerUser = async (user: IUser): Promise<any | undefined> => {
  try {
    const userModel = userEntity()
    return await userModel.create(user)
  } catch (error) {
    LogError(`[ORM ERROR]: Creating User: ${error}`)
  }
}

// Login User
export const loginUser = async (auth: IAuth): Promise<any | undefined> => {
  try {
    const userModel = userEntity()
    let userFound: IUser | undefined

    await userModel.findOne({ email: auth.email }).then((user: IUser) => {
      userFound = user
    }).catch((error) => {
      console.error('[ERROR Authentication in ORM]: User Not Found')
      throw new Error(`[ERROR Authentication in ORM]: User Not Found: ${error}`)
    })

    const validPassword = bcrypt.compareSync(auth.password, userFound!.password)

    if (!validPassword) {
      console.error('[ERROR Authentication in ORM]: Password Not Valid')
      throw new Error('[ERROR Authentication in ORM]: Password Not Valid')
    }

    const token = jwt.sign({ email: userFound!.email }, secret, { expiresIn: '2h' })

    return { user: userFound, token: token }
  } catch (error) {
    LogError(`[ORM ERROR]: Creating User: ${error}`)
  }
}

// Logout User
export const logoutUser = async (): Promise<any | undefined> => {

}
