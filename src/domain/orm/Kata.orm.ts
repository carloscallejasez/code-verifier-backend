import { kataEntity } from '../entities/Kata.entity';

import { LogSuccess, LogError } from "../../utils/logger";
import { IKata  } from "../interfaces/IKata.interface";

// Environment variables
import dotenv from 'dotenv';

// Configuration of environment variables
dotenv.config();

// CRUD

/**
 * Method to obtain all Katas from Collection "Katas" in Mongo Server
 */
export const getAllKatas = async (page: number, limit: number): Promise<any[] | undefined> => {
    try {
        let kataModel = kataEntity();

        let response: any = {};

        // Search all Katas (using pagination)
        await kataModel.find({isDeleted: false})
            .limit(limit)
            .skip((page - 1) * limit)
            .exec().then((katas: IKata[]) => {
                response.katas = katas;
            });
        
        // Count total documents in collection "Katas"
        await kataModel.countDocuments().then((total: number) => {
            response.totalPages = Math.ceil(total / limit);
            response.currentPage = page;
        });

        return response;

    } catch (error) {
        LogError(`[ORM ERROR]: Getting All Katas: ${error}`);
    }
}

// - Get Kata By ID
export const getKataById = async (id: string) : Promise<any | undefined> => {

    try {
        let kataModel = kataEntity();

        // Search Kata By ID
        return await kataModel.findById(id)

    } catch (error) {
        LogError(`[ORM ERROR]: Getting Kata By ID: ${error}`);
    }

}

// - Delete Kata By ID
export const deleteKata = async (id: string): Promise<any | undefined> => {

    try {
        let kataModel = kataEntity();

        // Delete Kata BY ID
        return await kataModel.deleteOne({ _id: id })

    } catch (error) {
        LogError(`[ORM ERROR]: Deleting Kata By ID: ${error}`);
    }

}

// - Create New Kata
export const createKata = async (kata: IKata): Promise<any | undefined> => {

    try {
        
        let kataModel = kataEntity();

        // Create / Insert new Kata
        return await kataModel.create(kata);

    } catch (error) {
        LogError(`[ORM ERROR]: Creating Kata: ${error}`);
    }

}

// - Update Kata By ID
export const updateKata = async (id: string, kata: IKata): Promise<any | undefined> => {

    try {
        
        let kataModel = kataEntity();

        // Update Kata
        return await kataModel.findByIdAndUpdate(id, kata);

    } catch (error) {
        LogError(`[ORM ERROR]: Updating Kata ${id}: ${error}`);
    }

}

/**
 * Method to filter katas by level from Katas Collection
 */
export const filterKataByLevel = async (level: number): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    return await kataModel.find({ level: level })
  } catch (error) {
    LogError(`[ORM ERROR]: Get Kata By Level ${level}: ${error}`)
  }
}
/**
 * Method to get 5 last katas from Katas Collection
 */
export const getLastFiveKatas = async (): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    return await kataModel.find().sort({ date: 1 }).limit(5)
  } catch (error) {
    LogError(`[ORM ERROR]: Get Last Five Katas: ${error}`)
  }
}
/**
 * Method to get katas by stars
 */
export const getByStars = async (stars:number): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    return await kataModel.find().sort({ stars: -1 })
  } catch (error) {
    LogError(`[ORM ERROR]: Get Katas By Valoration: ${error}`)
  }
}
/**
 * Method to update the Kata Valoration by ID
 */
export const updateStars = async (id: string, stars: number): Promise<any | undefined> => {
  const kataModel = kataEntity()
  kataModel.findOne({ _id: id },
    async function (err: any, result: any) {
      if (err) throw err
      try {
        const calculation1: any = Number(result.stars) * Number(result.numStars) + Number(stars)
        const calculation2: number = Number(result.numStars) + Number(1)
        const newValoration: number = calculation1 / calculation2
        return await kataModel.findByIdAndUpdate(id, { stras: newValoration, numValorations: result.numValorations + 1 })
      } catch (error) {
        LogError(`[ORM ERROR]: Updting Kata with ID: ${id} New valoration: ${error}`)
      }
    }
  )
}
/**
 * Method to filter katas by chances from Katas Collection
 */
export const getKataByIntents = async (intents: number): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    return await kataModel.find({ intents: intents })
  } catch (error) {
    LogError(`[ORM ERROR]: Get Kata by Intents ${intents}: ${error}`)
  }
}
