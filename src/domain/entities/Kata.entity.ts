import mongoose from 'mongoose'

export const kataEntity = () => {
  const kataSchema = new mongoose.Schema({
    name: String,
    description: String,
    level: Number,
    user: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Users'
    }],
    date: Date,
    valoration: Number,
    numValorations: Number,
    intents: Number
  })
  return mongoose.models.Katas || mongoose.model('Katas', kataSchema)
}
